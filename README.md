#NodeCL

- <kbd>WebSite</kbd>
	+ `./Web`	

- <kbd>es6 to es5</kbd>
	+ `./bin/es6to5`
	+ > \> cd .../bin/es6to5
	  > \> gulp

- <kbd>webpack</kbd>
	+ `./bin/webpack`
	+ > \> cd ./bin/webpack
	  > \> web --progress --colors --watch
	  > \> web -p --progress --colors --watch		//-p: 压缩
	+ https://github.com/webpack/docs/wiki/configuration#configuration-object-content `配置`

```package.json
"dependencies": {
    "sqlite3": "3.1.1"
},
"devDependencies": {
	"gulp": "~3.9.1",
	"gulp-babel": "6.1.2",
    "babel-preset-es2015": "6.9.0"
}
```
