'use strict';
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

module.exports = {
	WebInit: _init
}

function _init(indexStr){
	var urlencodedParser = bodyParser.urlencoded({ extended: false });

	app.get('/', function(req, res){
		res.sendFile(indexStr);
	});
	app.get('/process_get', urlencodedParser, function(req, res){
		var response = {
			name : req.query._name,	 
		};
		//console.log(response);
		res.header("Content-Type", "application/json; charset=utf-8");
		res.end("hello : " + response.name);
	});
	var server = app.listen(8080, function(){
		console.log('server start, listen:', server.address().port);
	});
}

