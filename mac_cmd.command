#! /bin/sh
# Copyright (c) 2000-2016 Cvjar

echo "git pull: '1'		git push: '2'"
path=$0
_path=${path:0:${#path}-16}
cd $_path
read flag

if [ ${flag} == '1' ]
then
	git fetch origin master
	git pull origin master
elif [ ${flag} == '2' ]
then
	git add .
	git status
	read commitstr
	git commit -m "${commitstr}"
	git push origin master
else
	echo 'Input error!'
fi
