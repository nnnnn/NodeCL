/*!
 * NodeCL 
 * Copyright(c) 2000-2016 Cvjar
 * MIT Licensed
 */

'use strict';

module.exports = require('./lib/nodecl')
