module.exports = {
    entry: [
        "./test",
        "./lib/Common",
        "./lib/ToolsFunction"
    ],
    output: {
        path: __dirname + "/dist",
        filename: "bundle.js"
    }
    // module: {
    //     loaders: [
    //         { test: /\.css$/, loader: "style!css" }
    //     ]
    // }
};